## Healthy Helper

Healthy Helper is an application for anyone wanting to cook and eat healthy. This application allows you to search from thousands of nutritional recipes, save the ingredients to a grocery list, and even calculate the macronutrients from the recipe ingredients. While Healthy Helper does not guide the user on cooking these recipes, it does provide you with the link to more detailed information from FatSecret.com [API source].

API: http://platform.fatsecret.com/api/

This application uses the FatSecret Platform API. In conjunction with FatSecret Platform API Terms of Use, by using this application you are agreeing to be bound by the FatSecret Platform API Terms of Use, which can be found here:
http://platform.fatsecret.com/api/Default.aspx?screen=tou.


### Requirements
- Android device running API 25 or higher
- Valid internet connection


#### Known Bugs
Due to FatSecret’s custom API Library issues, some recipe results will be unavailable due to JSON parsing errors. I have made it so the application will not crash, at least, however some of these errors are out of my hands. The developers at FatSecret have already been made aware of this issue.


### Change Log
- Nutrition aspects of this application were removed


### Developed by Zach Wilkin for Project & Portfolio VI
