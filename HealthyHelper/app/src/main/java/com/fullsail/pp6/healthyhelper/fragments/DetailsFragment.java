package com.fullsail.pp6.healthyhelper.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullsail.pp6.healthyhelper.R;
import com.fullsail.pp6.healthyhelper.classes.ResultRecipe;
import com.fullsail.pp6.healthyhelper.helpers.IOHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


// Zach Wilkin
// PP6 - 1709
// DetailsFragment.java

public class DetailsFragment extends Fragment implements View.OnClickListener {


    // Static Constant Variables
    public static final String TAG = "DetailsFragment.TAG";
    private static final String BUNDLE_RECIPE = "RESULT_RECIPE_BUNDLE_KEY";

    // Member Variables
    static ResultRecipe mRecipe;
    private static OnButtonClicked mListener;


    // New Instance
    public static DetailsFragment newInstance(ResultRecipe recipe) {
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_RECIPE, recipe);

        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }



    // Interface
    public interface OnButtonClicked {
        void onButtonClicked(int btn_id, ResultRecipe recipe);
    }


    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onButtonClicked(v.getId(), mRecipe);
            if (v.getId() == R.id.btn_add_favorites) ((Button) v).setText(R.string.saved);
            else if (v.getId() == R.id.btn_add_grocery) ((Button) v).setText(R.string.added);

            v.setEnabled(false);
        }
    }

    // Lifecycle
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnButtonClicked) mListener = (OnButtonClicked) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Retrieve the recipe sent through the Fragment's arguments
        mRecipe = (ResultRecipe) getArguments().getSerializable(BUNDLE_RECIPE);

        if (getView() != null && mRecipe != null) {
            // Load Details
            // Image
            if (mRecipe.getImage().isEmpty())
                ((ImageView) getView().findViewById(R.id.details_image)).setImageResource(R.drawable.ic_no_image);
            else
                Picasso.with(getContext())
                        .load(mRecipe.getImage())
                        .fit()
                        .into((ImageView) getView().findViewById(R.id.details_image));

            // Recipe Title / Description / Calories
            ((TextView) getView().findViewById(R.id.details_title)).setText(mRecipe.getName());
            ((TextView) getView().findViewById(R.id.details_description)).setText(mRecipe.getDescription());
            ((TextView) getView().findViewById(R.id.details_calories)).setText(String.valueOf(mRecipe.getMacronutrients().getCalories()));

            // Ingredients
            TextView tv = (TextView) getView().findViewById(R.id.details_ingredients);
            tv.setText("");
            for (String ingredient : mRecipe.getIngredients())
                if (tv.getText().toString().isEmpty()) tv.setText(ingredient);
                else tv.setText(tv.getText().toString() + "\n" + ingredient);

            // Buttons
            getView().findViewById(R.id.btn_add_grocery).setOnClickListener(this);
            getView().findViewById(R.id.btn_add_favorites).setOnClickListener(this);

            // Check if mRecipe is a saved favorite
            ArrayList<ResultRecipe> favorites = IOHelper.loadFavorites(getContext());
            if (!favorites.isEmpty())
                favorites.stream().filter(r -> r.getID().equals(mRecipe.getID())).forEach(r -> {
                    getView().findViewById(R.id.btn_add_favorites).setEnabled(false);
                    ((Button) getView().findViewById(R.id.btn_add_favorites)).setText(R.string.saved);
                });
        }
    }
}
