package com.fullsail.pp6.healthyhelper.search;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.fatsecret.platform.services.RequestBuilder;
import com.fullsail.pp6.healthyhelper.classes.Macronutrients;
import com.fullsail.pp6.healthyhelper.classes.ResultRecipe;
import com.fullsail.pp6.healthyhelper.helpers.RecipeHelper;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


// Zach Wilkin
// PP6 - 1709
// DownloadTask.java

public class DownloadTask extends AsyncTask<String, ResultRecipe, Void> {

    // FatSecret API Keys
    private static final String api_key = "d40b10090e9747e186adff740bb6e0c5";
    private static final String api_secret = "95b500d8833a4fa4bc0e5e0e52b982ae";


    // Member Variables
    private static final String TAG = "DownloadTask.TAG";
    private final Context mContext;
    private final OnDownloadFinished mFinished;
    private RequestBuilder mBuilder;


    // Constructor
    public DownloadTask(Context context) {
        mContext = context;
        mFinished = (OnDownloadFinished) context;
    }


    // Interface
    public interface OnDownloadFinished {
        void onRecipeDownloaded(ResultRecipe recipe);
    }


    // Lifecycle
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        // Set up the RequestBuilder
        mBuilder = new RequestBuilder(api_key, api_secret);
    }


    @Override
    protected void onProgressUpdate(ResultRecipe... recipe) {
        super.onProgressUpdate(recipe);
        mFinished.onRecipeDownloaded(recipe[0]);
    }

    @Override
    protected Void doInBackground(String... query) {
        // Create temporary lists to be populated later
        List<Long> temp_recipe_ids = new ArrayList<>();
        List<ResultRecipe> downloaded_recipes = new ArrayList<>();

        // Part 1
        ////////////////////////////////////////////////////////////////////////////////
        /* 1. Get a list of recipe results (ID's) */
        try {
            // Obtain the API URL
            URL url = new URL(mBuilder.buildRecipesSearchUrl(query[0], 0));
            Log.i(TAG, "doInBackground: " + url);

            // Retrieve the JSON data from the URL
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            String recipes_json = IOUtils.toString(connection.getInputStream());

            // Close the open connection
            connection.disconnect();

            temp_recipe_ids = RecipeHelper.getIdsFromJSON(recipes_json);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "doInBackground: There was an error parsing the Recipe Result ID's",
                    new JSONException("Error parsing the Recipe ID's"));
        }
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////


        // Part 2
        ////////////////////////////////////////////////////////////////////////////////
        /* 2. Loop through saved ID's and obtain ResultRecipe required information */
        if (!temp_recipe_ids.isEmpty()) {
            for (int i = 0; i < 20; i++) {
                try {
                    // Obtain the API URL
                    URL url = new URL(mBuilder.buildRecipeGetUrl(temp_recipe_ids.get(i)));

                    // Retrieve JSON data from the URL
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    String recipe_json = IOUtils.toString(connection.getInputStream());

                    // Close the open connection
                    connection.disconnect();

                    // Retrieve the JSON Objects
                    JSONObject json_data = new JSONObject(recipe_json);
                    JSONObject recipe_obj = json_data.getJSONObject("recipe");


                    // New ResultRecipe Class
                    ResultRecipe recipe = new ResultRecipe();

                    // Set up the ResultRecipe details
                    // 1. Name
                    recipe.setName(recipe_obj.getString("recipe_name"));
                    // 2. ID
                    recipe.setID(temp_recipe_ids.get(i));
                    // 3. Description
                    recipe.setDescription(recipe_obj.getString("recipe_description"));
                    // 4. Image (validate first)
                    if (recipe_obj.has("recipe_images")) {
                        JSONObject obj = recipe_obj.getJSONObject("recipe_images");
                        recipe.setImage(obj.getString("recipe_image"));
                    }
                    // 5. Ingredients
                    JSONObject ingredients = recipe_obj.getJSONObject("ingredients");
                    JSONArray ingredient_arr = ingredients.getJSONArray("ingredient");

                    // Loop through ingredients and add to the ResultRecipe's list
                    for (int a = 0; a < ingredient_arr.length(); a++) {
                        JSONObject obj = ingredient_arr.getJSONObject(a);
                        if (obj.getString("food_name").startsWith("Chicken Breast Meat")) {
                            recipe.addIngredient("Chicken Breast Meat");
                        } else {
                            recipe.addIngredient(obj.getString("food_name"));
                        }
                    }

                    // 6. Macronutrients
                    JSONObject serving_sizes = recipe_obj.getJSONObject("serving_sizes");
                    JSONObject serving_obj = serving_sizes.getJSONObject("serving");

                    recipe.setMacronutrients(new Macronutrients(
                            serving_obj.getString("calories"),
                            serving_obj.getString("carbohydrate"),
                            serving_obj.getString("fat"),
                            serving_obj.getString("protein")
                    ));

                    // Publish the new ResultRecipe
                    publishProgress(recipe);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Log.e(TAG, "doInBackground: An error has occurred when ",
                    new JSONException(
                            "An unknown error has occurred, causing the 'temp_recipe_ids' " +
                            "variable to be unpopulated. \nPlease inform the developer about this error " +
                            "regarding to 135 of DownloadTask.java"));
        }
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////

        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        super.onPostExecute(v);
        Toast.makeText(mContext, "Recipes Found!", Toast.LENGTH_SHORT).show();
    }
}
