package com.fullsail.pp6.healthyhelper.classes;

// Zach Wilkin
// PP6 - 1709
// ResultRecipe.java

import java.io.Serializable;
import java.util.ArrayList;

public class ResultRecipe implements Serializable {

    // Recipe Variables
    private String mName;
    private Long mId;
    private String mDescription;
    private String mImage;
    private ArrayList<String> mIngredients;
    private Macronutrients mMacronutrients;

    // Default Constructor
    public ResultRecipe(){
        mIngredients = new ArrayList<>();
    }

    // Constructor
    public ResultRecipe(String name, Long id, String description, String image, ArrayList<String> ingredients,
                 String calories, String carbs, String fat, String protein) {
        this.mName = name;
        this.mId = id;
        this.mDescription = description;
        this.mImage = image;
        this.mIngredients = ingredients;
        this.mMacronutrients = new Macronutrients(calories, carbs, fat, protein);
    }

    // Add item to Ingredients
    public void addIngredient(String ingredient) {
        mIngredients.add(ingredient);
    }

    // Setters
    public void setName(String name) {
        this.mName = name;
    }
    public void setID(Long id) {
        this.mId = id;
    }
    public void setDescription(String description) {
        this.mDescription = description;
    }
    public void setImage(String image) {
        this.mImage = image;
    }
    public void setIngredients(ArrayList<String> ingredients) {
        this.mIngredients = ingredients;
    }
    public void setMacronutrients(Macronutrients macros) {
        this.mMacronutrients = macros;
    }

    // Getters
    public String getName() {
        return mName;
    }
    public Long getID() {
        return mId;
    }
    public String getDescription() {
        return mDescription;
    }
    public String getImage() {
        if (mImage == null) return "";
        return mImage;
    }
    public ArrayList<String> getIngredients() {
        return mIngredients;
    }
    public Macronutrients getMacronutrients() {
        return mMacronutrients;
    }
}
