package com.fullsail.pp6.healthyhelper.fragments;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.fullsail.pp6.healthyhelper.R;
import com.fullsail.pp6.healthyhelper.classes.GroceryAdapter;
import com.fullsail.pp6.healthyhelper.helpers.IOHelper;

import java.util.ArrayList;

import static com.fullsail.pp6.healthyhelper.MainActivity.checkedGroceries;


// Zach Wilkin
// PP6 - 1709
// GroceryFragment.java

public class GroceryFragment extends ListFragment {

    // New Instance
    public static GroceryFragment newInstance() {
        return new GroceryFragment();
    }

    // Static Constant Variables
    public static final String TAG = "GroceryFragment.TAG";

    // Member Variables
    public static boolean currentlyShopping = false;
    GroceryAdapter mAdapter = null;
    ArrayList<String> mGroceries;


    // Set up Menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.grocery_menu, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!currentlyShopping) {
            currentlyShopping = true;
            item.setIcon(R.drawable.ic_grocery_light);
            mAdapter.notifyDataSetChanged();
        } else {
            // Disable 'Shopping Mode' if no items are checked; else verify deletion with the user
            if (checkedGroceries.isEmpty()) {
                currentlyShopping = false;
                mAdapter.notifyDataSetChanged();
                item.setIcon(R.drawable.ic_grocery);
            } else {
                // Verify with the user to delete the selected groceries (before deleting)
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog);
                builder.setTitle(R.string.grocery_update_title);
                builder.setMessage(R.string.grocery_update_msg);
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Delete the selected groceries from the user's saved list
                        for (String grocery : checkedGroceries) {
                            IOHelper.deleteGrocery(getContext(), grocery);
                            mGroceries.remove(grocery);
                            Log.i(TAG, "onOptionsItemSelected: Deleted - " + grocery + ".");
                        }
                        checkedGroceries.clear();
                        mAdapter.notifyDataSetChanged();
                        currentlyShopping = false;
                        item.setIcon(R.drawable.ic_grocery);
                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    // Lifecycle
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up menu
        setHasOptionsMenu(true);

        // Load in saved groceries
        mGroceries = IOHelper.loadGroceries(getContext());

        // Set up GroceryAdapter
        mAdapter = new GroceryAdapter(mGroceries, getContext());
        setListAdapter(mAdapter);
    }

}
