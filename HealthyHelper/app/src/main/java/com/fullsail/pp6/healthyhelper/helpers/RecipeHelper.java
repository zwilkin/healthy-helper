package com.fullsail.pp6.healthyhelper.helpers;


// Zach Wilkin
// PP6 - 1709
// RecipeHelper.java

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RecipeHelper {


    /**
     * Returns a list of recipe id's to be used for looking up additional information about that recipe.
     *
     * @param data The JSON data to be parsed through
     * @return List of ids as {@link Long}
     * @throws JSONException
     */
    public static List<Long> getIdsFromJSON(String data) throws JSONException {
        List<Long> ids = new ArrayList<>();

        // Retrieve the JSON Objects
        JSONObject json_data = new JSONObject(data);
        JSONObject recipes_obj = json_data.getJSONObject("recipes");

        try {
            JSONArray recipes_arr = recipes_obj.getJSONArray("recipe");

            // Loop through recipes and add their ID's to the list
            for (int i = 0; i < 20; i++) {
                JSONObject obj = recipes_arr.getJSONObject(i);
                ids.add(obj.getLong("recipe_id"));
            }

        } catch (JSONException e) {
            e.printStackTrace();

            // Only 1 recipe was returned (therefore Object, not Array)
            JSONObject recipe_obj = recipes_obj.getJSONObject("recipe");
            ids.add(Long.parseLong(recipe_obj.getString("recipe_id")));
        }

        return ids;
    }

}
