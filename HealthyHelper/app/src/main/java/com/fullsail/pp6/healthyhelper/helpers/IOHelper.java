package com.fullsail.pp6.healthyhelper.helpers;


// Zach Wilkin
// PP6 - 1709
// IOHelper.java

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.fullsail.pp6.healthyhelper.classes.ResultRecipe;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class IOHelper {

    // Static Variables
    private static final String TAG = "IOHelper.TAG";
    private static final String FILE_FAVORITES = "favorite_recipes.bin";
    private static final String FILE_GROCERIES = "grocery_list.bin";


    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    // Favorites

    // Save a Single Recipe
    public static void saveFavorite(Context context, ResultRecipe recipe) {
        ArrayList<ResultRecipe> recipes = loadFavorites(context);
        for (ResultRecipe r : recipes) if (recipe.equals(r)) return;
        recipes.add(recipe);
        saveFavorites(context, recipes);
    }
    // Save Multiple Recipes
    public static void saveFavorites(Context context, ArrayList<ResultRecipe> recipes) {
        try {
            FileOutputStream fos = context.openFileOutput(FILE_FAVORITES, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(recipes);

            oos.close();
            fos.close();
        } catch (IOException e) {e.printStackTrace();}
    }

    // Load Favorites
    @SuppressWarnings("unchecked")
    @NonNull
    public static ArrayList<ResultRecipe> loadFavorites(Context context) {
        ArrayList<ResultRecipe> favorites = null;

        try {
            FileInputStream fis = context.openFileInput(FILE_FAVORITES);
            ObjectInputStream ois = new ObjectInputStream(fis);

            favorites = (ArrayList<ResultRecipe>) ois.readObject();

            ois.close();
            fis.close();
        } catch (FileNotFoundException ignored){ // No favorites have been saved - handled later
        } catch (Exception e) {e.printStackTrace();}

        if (favorites == null) favorites = new ArrayList<>();

        return favorites;
    }

    // Delete Recipe
    public static void deleteFavorite(Context context, ResultRecipe recipe) {
        ArrayList<ResultRecipe> recipes = loadFavorites(context);
        while (true) if (!(recipes.remove(recipe))) break;
        saveFavorites(context, recipes);
        Toast.makeText(context, "Removed " + recipe.getName(), Toast.LENGTH_SHORT).show();
    }
    ////////////////////////////////////////////////////////////////////////////////




    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    // Grocery List

    // Save a Single Grocery
    public static void saveGrocery(Context context, String grocery) {
        ArrayList<String> groceries = loadGroceries(context);
        for (String g : groceries) if (grocery.equals(g)) return;
        groceries.add(grocery);
        saveGroceries(context, groceries);
    }
    // Save Multiple Groceries
    private static void saveGroceries(Context context, ArrayList<String> groceries) {
        try {
            FileOutputStream fos = context.openFileOutput(FILE_GROCERIES, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(groceries);

            oos.close();
            fos.close();
        } catch (IOException e) {e.printStackTrace();}
    }

    // Load Groceries
    @SuppressWarnings("unchecked")
    @NonNull
    public static ArrayList<String> loadGroceries(Context context) {
        ArrayList<String> groceries = null;

        try {
            FileInputStream fis = context.openFileInput(FILE_GROCERIES);
            ObjectInputStream ois = new ObjectInputStream(fis);

            groceries = (ArrayList<String>) ois.readObject();

            ois.close();
            fis.close();
        } catch (FileNotFoundException ignored){ // No favorites have been saved - handled later
        } catch (Exception e) {e.printStackTrace();}

        if (groceries == null) groceries = new ArrayList<>();

        return groceries;
    }

    // Delete Grocery
    public static void deleteGrocery(Context context, String grocery) {
        ArrayList<String> groceries = loadGroceries(context);
        while (true) if (!(groceries.remove(grocery))) break;
        saveGroceries(context, groceries);
    }
    ////////////////////////////////////////////////////////////////////////////////
}
