package com.fullsail.pp6.healthyhelper.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fullsail.pp6.healthyhelper.R;
import com.fullsail.pp6.healthyhelper.classes.ResultRecipe;
import com.fullsail.pp6.healthyhelper.helpers.IOHelper;
import com.fullsail.pp6.healthyhelper.helpers.NavHelper;
import com.fullsail.pp6.healthyhelper.search.RecipeResultAdapter;
import com.github.brnunes.swipeablerecyclerview.SwipeableRecyclerViewTouchListener;

import java.util.ArrayList;
import java.util.List;

import static com.fullsail.pp6.healthyhelper.helpers.NavHelper.RESULT_KEY;


// Zach Wilkin
// PP6 - 1709
// ResultsFragment.java

public class ResultsFragment extends Fragment {

    // New Instance
    public static ResultsFragment newInstance(@NonNull NavHelper.ResultType rt) {
        Bundle args = new Bundle();
        args.putSerializable(RESULT_KEY, rt);

        ResultsFragment fragment = new ResultsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    // Static Constant Variables
    public static final String TAG = "ResultsFragment.TAG";

    // Member Variables
    private OnResultsDisplayed mListener;
    private RecipeResultAdapter mAdapter;

    // Results Listener Interface
    public interface OnResultsDisplayed {
        List<ResultRecipe> retrieveRecipeResults();
        RecipeResultAdapter getResultAdapter();
    }



    // Lifecycle
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnResultsDisplayed) mListener = (OnResultsDisplayed) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_results, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getView() == null || mListener == null) return;

        // Set up Results (Card Views)
        RecyclerView recycler = (RecyclerView) getView().findViewById(R.id.recipe_recycler);
        recycler.setHasFixedSize(true);

        // Set up the layout
        RecyclerView.LayoutManager layoutMgr = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(layoutMgr);

        // Determine which results should be in the list to be displayed based on the provided ResultType
        NavHelper.ResultType rt = (NavHelper.ResultType) getArguments().getSerializable(RESULT_KEY);
        if (rt != null) switch (rt) {
            case SEARCH:
                ((TextView) getView().findViewById(R.id.result_title)).setText(R.string.search_results);
                recycler.setAdapter(mListener.getResultAdapter());
//                mAdapter = new RecipeResultAdapter(getContext(), mListener.retrieveRecipeResults());
//                recycler.setAdapter(new RecipeResultAdapter(getContext(), mListener.retrieveRecipeResults()));
                break;
            case FAVORITES:
                ((TextView) getView().findViewById(R.id.result_title)).setText(R.string.favorites);
                ArrayList<ResultRecipe> recipes = IOHelper.loadFavorites(getContext());

                RecipeResultAdapter adapter = new RecipeResultAdapter(getContext(), recipes);
                recycler.setAdapter(adapter);

                recycler.addOnItemTouchListener(new SwipeableRecyclerViewTouchListener(
                        recycler, new SwipeableRecyclerViewTouchListener.SwipeListener() {
                    @Override
                    public boolean canSwipeLeft(int position) {
                        return true;
                    }
                    @Override
                    public boolean canSwipeRight(int position) {
                        return false;
                    }

                    @Override
                    public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
                        // Verify with user to delete the swiped recipe
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), 0);
                        builder.setTitle("Delete?")
                                .setMessage("Are you sure you wish to delete this favorite?")
                                .setNegativeButton("No", null)
                                .setPositiveButton("Yes", (dialog, which) -> {
                                    recipes.remove(reverseSortedPositions[0]);
                                    IOHelper.saveFavorites(getContext(), recipes);

                                    adapter.notifyItemRemoved(reverseSortedPositions[0]);
                                    adapter.notifyDataSetChanged();
                                });
                        builder.show();
                    }

                    @Override
                    public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
                    }
                }));

                break;
            default: break;
        } else
            throw new IllegalArgumentException("There was no valid ResultType passed through the " +
                    "Fragment's arguments. Please use NavHelper.RESULT_KEY to pass this parameter");


    }
}
