package com.fullsail.pp6.healthyhelper.helpers;


// Zach Wilkin
// PP6 - 1709
// ConnectionHelper.java

import android.content.Context;
import android.net.ConnectivityManager;

public class ConnectionHelper {

    /**
     * Checks the {@link ConnectivityManager} of the given {@link Context}
     * for a valid internet connection.
     *
     * @param context The context of where the {@link ConnectivityManager} is retrieved from.
     * @return True if a valid connection is established.
     */
    public static boolean validConnection(Context context) {
        ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (mgr != null) {
            if (mgr.getActiveNetworkInfo() != null
                    && mgr.getActiveNetworkInfo().isConnected()
                    && mgr.getActiveNetworkInfo().isAvailable()) {

                // Valid Connection
                return true;
            }
        }
        // Invalid Connection
        return false;
    }

}
