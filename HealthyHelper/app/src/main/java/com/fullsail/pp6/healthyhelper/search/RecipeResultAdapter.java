package com.fullsail.pp6.healthyhelper.search;


// Zach Wilkin
// PP6 - 1709
// RecipeResultAdapter.java

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fullsail.pp6.healthyhelper.R;
import com.fullsail.pp6.healthyhelper.classes.ResultRecipe;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class RecipeResultAdapter extends RecyclerView.Adapter<RecipeResultAdapter.ResultsViewHolder> {

    // Constant Static Variables
    public static final String TAG = "ResultsAdapter.TAG";

    // Member Variables
    private Context mContext;
    private List<ResultRecipe> mRecipeResults;
    private OnRecipeCardClicked mListener;


    // Constructor
    public RecipeResultAdapter(Context context, List<ResultRecipe> recipes) {
        this.mContext = context;
        this.mRecipeResults = recipes;
        if (context instanceof OnRecipeCardClicked) mListener = (OnRecipeCardClicked) context;
    }

    // Public Interface
    public interface OnRecipeCardClicked {
        void recipeCardViewClicked(ResultRecipe recipe);
    }

    // Adapter Instance Methods
    @Override
    public ResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.card_recipe, parent, false);
        return new ResultsViewHolder(mView);
    }

    // Occurs when the first instance of a new view is going to be shown (i.e. 'recycled')
    @Override
    public void onBindViewHolder(ResultsViewHolder holder, int position) {

        // Check if the recipe has any associated images first
        if (mRecipeResults.get(position).getImage().isEmpty()) {
            // No images to be displayed
            holder.recipe_image
                    .setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_no_image));
        } else {
            // Use Picasso to load the recipe image into the view
            Picasso.with(mContext)
                    .load(mRecipeResults.get(position).getImage())
                    .fit()
                    .into(holder.recipe_image);
        }

        // Set the Recipe Title and Description
        holder.recipe_title.setText(mRecipeResults.get(position).getName());
        holder.recipe_description.setText(mRecipeResults.get(position).getDescription());

        // Format the calories with proper decimal
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String calText = formatter.format(mRecipeResults.get(position).getMacronutrients().getCalories()) + " calories per serving";
        holder.recipe_calories.setText(calText);

        // Set up the ClickListener
        holder.recipe_card_view.setOnClickListener(v -> {
            if (mListener != null) mListener.recipeCardViewClicked(mRecipeResults.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return mRecipeResults.size();
    }


    // Custom ViewHolder class
    static class ResultsViewHolder extends RecyclerView.ViewHolder {

        // UI Element Variables
        ImageView recipe_image;
        TextView recipe_title;
        TextView recipe_description;
        TextView recipe_calories;
        TextView recipe_ingredients;
        CardView recipe_card_view;


        ResultsViewHolder(View itemView) {
            super(itemView);

            // Set up UI Element Variables
            recipe_image = (ImageView) itemView.findViewById(R.id.recipe_image);
            recipe_title = (TextView) itemView.findViewById(R.id.recipe_title);
            recipe_description = (TextView) itemView.findViewById(R.id.recipe_description);
            recipe_calories = (TextView) itemView.findViewById(R.id.recipe_calories);
            recipe_card_view = (CardView) itemView.findViewById(R.id.recipe_card_view);
        }
    }
}
