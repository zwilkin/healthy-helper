package com.fullsail.pp6.healthyhelper.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.fullsail.pp6.healthyhelper.R;


// Zach Wilkin
// PP6 - 1709
// SearchFragment.java

public class SearchFragment extends Fragment {

    // New Instance
    public static SearchFragment newInstance() {
        return new SearchFragment();
    }


    // Static Constant Variables
    public static final String TAG = "SearchFragment.TAG";

    // Member Variables
    private OnRecipeSearch mListener;


    // Search Listener Interface
    public interface OnRecipeSearch {
        void onRecipeSearch(String query);
    }




    // Lifecycle
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRecipeSearch) mListener = (OnRecipeSearch) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_search, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null) {
            getView().findViewById(R.id.btn_search).setOnClickListener(v -> {
                if (mListener != null) mListener.onRecipeSearch(
                        ((EditText) getView().findViewById(R.id.et_search)).getText().toString().trim()
                );
            });
        }
    }


}
