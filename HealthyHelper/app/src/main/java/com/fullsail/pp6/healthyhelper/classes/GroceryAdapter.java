package com.fullsail.pp6.healthyhelper.classes;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.fullsail.pp6.healthyhelper.R;

import java.util.ArrayList;

import static com.fullsail.pp6.healthyhelper.fragments.GroceryFragment.currentlyShopping;


// Zach Wilkin
// PP6 - 1709
// GroceryAdapter.java

public class GroceryAdapter extends BaseAdapter {


    // Static Constants
    private static final String TAG = "GroceryAdapter.TAG";
    private static final int base_ID = 0x05013;
    private final ArrayList<String> mGroceries;
    private Context mContext;
    private OnGroceryChecked mChecker;


    // Public Constructor
    public GroceryAdapter(ArrayList<String> groceries, Context context) {
        mGroceries = groceries;
        mContext = context;
        mChecker = (OnGroceryChecked) context;
    }


    // Checkbox Interface
    public interface OnGroceryChecked {
        void onGroceryItemChecked(String grocery, int position);
        void onGroceryItemUnchecked(String grocery, int position);
    }

    // Override Methods
    @Override
    public int getCount() {
        if (mGroceries != null) return mGroceries.size();
        else return 0;
    }

    @Override
    public String getItem(int position) {
        if (mGroceries != null && position >= 0 && position < mGroceries.size()) return mGroceries.get(position);
        else return null;
    }

    @Override
    public long getItemId(int position) {
        if (mGroceries != null && position >= 0 && position < mGroceries.size()) return base_ID + position;
        else Log.e(TAG, "getItemId: There was a problem assigning an ID!"); return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Check if convertView exists before proceeding
        if (convertView == null)
            convertView = LayoutInflater.from(mContext).inflate(R.layout.cell_grocery, parent, false);

        // Set up Ingredient label & Checkbox
        ((TextView) convertView.findViewById(R.id.tv_ingredient)).setText(getItem(position));
        CheckBox cB = (CheckBox) convertView.findViewById(R.id.checkBox);
        if (cB.isChecked()) cB.setChecked(false);

        if (!currentlyShopping) {
            cB.setVisibility(View.INVISIBLE);
        } else {
            cB.setVisibility(View.VISIBLE);

            cB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    buttonView.setChecked(isChecked);

                    if (isChecked) {
                        // Grocery selected
                        if (mChecker != null)
                            mChecker.onGroceryItemChecked(mGroceries.get(position), position);
                    } else {
                        // Grocery deselected
                        if (mChecker != null)
                            mChecker.onGroceryItemUnchecked(mGroceries.get(position), position);
                    }
                }
            });
        }

        return convertView;
    }



}
