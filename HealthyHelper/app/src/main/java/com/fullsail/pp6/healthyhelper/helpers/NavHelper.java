package com.fullsail.pp6.healthyhelper.helpers;


// Zach Wilkin
// PP6 - 1709
// NavHelper.java

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.fullsail.pp6.healthyhelper.R;
import com.fullsail.pp6.healthyhelper.fragments.GroceryFragment;
import com.fullsail.pp6.healthyhelper.fragments.NoGroceryFragment;
import com.fullsail.pp6.healthyhelper.fragments.ResultsFragment;
import com.fullsail.pp6.healthyhelper.fragments.SearchFragment;

import java.util.ArrayList;

/**
 * Helper class used for handling Navigation {@link DrawerLayout} selections.
 */
public class NavHelper {

    private static final String TAG = "NavHelper.TAG";

    // Fragment Backstack Variables
    public static final String BACKSTACK_SEARCH = "com.fullsail.pp6.healthyhelper.BACKSTACK_SEARCH";
    public static final String BACKSTACK_FAVORITES = "com.fullsail.pp6.healthyhelper.BACKSTACK_FAVORITES";
    public static final String BACKSTACK_GROCERY = "com.fullsail.pp6.healthyhelper.BACKSTACK_GROCERY";
    public static final String BACKSTACK_SHARE = "com.fullsail.pp6.healthyhelper.BACKSTACK_SHARE";



    /**
     * Helper method to create an {@link ActionBarDrawerToggle} from the given {@link AppCompatActivity}.
     *
     * @param activity The activity where the {@link DrawerLayout} resides.
     * @return {@link ActionBarDrawerToggle} set up from the passed in {@link AppCompatActivity}
     */
    @NonNull
    public static ActionBarDrawerToggle setUpNavDrawerToggle(@NonNull AppCompatActivity activity) {
        // Set up the DrawerLayout
        DrawerLayout layout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);

        // Create the ActionBarDrawerToggle
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity, layout, R.string.open, R.string.close);

        // Set Toggle to the Layout
        layout.addDrawerListener(toggle);

        // Set the Navigation Drawer to be displayed
        if (activity.getSupportActionBar() != null)
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return toggle;
    }


    /**
     * Public Enum for determining which recipes to populate for a new {@link ResultsFragment}.
     */
    public enum ResultType {SEARCH, FAVORITES}
    public static final String RESULT_KEY = "com.fullsail.pp6.healthyhelper.KEY_RESULT_TYPE";


    /**
     * <p>
     *     Helper method to load the selected screen from the Navigation Drawer using the
     *     {@link android.app.FragmentManager FragmentManager} that is provided by the given {@link Activity}.
     * </p>
     *
     * <p>
     *     This method only determines and loads the proper fragment of the desired selection - the
     *     rest of the Fragment's activity is handled by the specific Fragment.
     * </p>
     *
     * @param activity The {@link Activity} of where the {@link android.app.FragmentManager FragmentManager} will be obtained.
     * @param item The {@link MenuItem} in the Navigation Drawer that was selected.
     * @param drawer The {@link DrawerLayout} of the main Navigation Drawer.
     * @return True if the action has been successfully completed.
     */
    public static boolean drawerItemSelected(Activity activity, MenuItem item, DrawerLayout drawer) {

        switch (item.getItemId()) {
            case R.id.nav_search:
                // Load Search Fragment
                activity.getFragmentManager().beginTransaction()
                        .replace(R.id.container,
                                SearchFragment.newInstance(),
                                SearchFragment.TAG)
                        .commit();
                break;
            case R.id.nav_favorites:
                // Load Favorites Fragment
                activity.getFragmentManager().beginTransaction()
                        .replace(R.id.container,
                                ResultsFragment.newInstance(ResultType.FAVORITES),
                                ResultsFragment.TAG)
                        .commit();
                break;
            case R.id.nav_grocery:
                // Check if the user has added any groceries to their list,
                ArrayList<String> groceries = IOHelper.loadGroceries(activity);

                if (groceries.isEmpty()) {
                    // Load Empty Groceries Fragment
                    activity.getFragmentManager().beginTransaction()
                            .replace(R.id.container,
                                    NoGroceryFragment.newInstance())
                            .commit();
                } else {
                    // Load the Grocery List Fragment
                    activity.getFragmentManager().beginTransaction()
                            .replace(R.id.container,
                                    GroceryFragment.newInstance(),
                                    GroceryFragment.TAG)
                            .commit();
                }
                break;
            default:
                throw new IllegalArgumentException("An invalid MenuItem was provided.");
        }
        // Make sure to close the drawer when finished handling the selection
        drawer.closeDrawers();
        return true;
    }


}