package com.fullsail.pp6.healthyhelper.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fullsail.pp6.healthyhelper.R;


// Zach Wilkin
// PP6 - 1709
// InternetFragment.java

public class InternetFragment extends Fragment {

    public static InternetFragment newInstance() {
        return new InternetFragment();
    }


    // Lifecycle
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_internet_required, container, false);
    }

}
