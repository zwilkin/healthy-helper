// Zach Wilkin
// PP6 - 1709
// MainActivity.java

package com.fullsail.pp6.healthyhelper;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.fullsail.pp6.healthyhelper.classes.GroceryAdapter;
import com.fullsail.pp6.healthyhelper.classes.ResultRecipe;
import com.fullsail.pp6.healthyhelper.fragments.DetailsFragment;
import com.fullsail.pp6.healthyhelper.fragments.InternetFragment;
import com.fullsail.pp6.healthyhelper.fragments.ResultsFragment;
import com.fullsail.pp6.healthyhelper.fragments.SearchFragment;
import com.fullsail.pp6.healthyhelper.helpers.ConnectionHelper;
import com.fullsail.pp6.healthyhelper.helpers.IOHelper;
import com.fullsail.pp6.healthyhelper.helpers.NavHelper;
import com.fullsail.pp6.healthyhelper.search.DownloadTask;
import com.fullsail.pp6.healthyhelper.search.RecipeResultAdapter;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchFragment.OnRecipeSearch,
        ResultsFragment.OnResultsDisplayed, RecipeResultAdapter.OnRecipeCardClicked, DownloadTask.OnDownloadFinished,
        DetailsFragment.OnButtonClicked, GroceryAdapter.OnGroceryChecked {

    // Static Constant Variables
    public static final String TAG = "MainActivity.TAG";


    // Member Variables - sorry Celey :(
    List<ResultRecipe> mRecipeResults;
    ActionBarDrawerToggle mNavToggle;
    RecipeResultAdapter resultAdapter;
    public static List<String> checkedGroceries;


    // Activity Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkedGroceries = new ArrayList<>();

        // Set up the Nav Drawer Toggle
        mNavToggle = NavHelper.setUpNavDrawerToggle(this);
        mNavToggle.syncState();

        // Set up the NavigationView
        NavigationView navView = (NavigationView) findViewById(R.id.nav_drawer_view);
        if (navView != null) navView.setNavigationItemSelectedListener(this);


        // Check for valid internet connection before continuing
        if (ConnectionHelper.validConnection(this)) {
            // Programmatically select the Nav_Search item to display the Search Fragment
            if (navView != null) navView.getMenu().performIdentifierAction(R.id.nav_search, 0);
        } else {
            // Display fragment notifying the user that internet permissions are required
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, InternetFragment.newInstance())
                    .commit();

            // Notify the user that they require internet connection
            Toast.makeText(this, R.string.internet_required, Toast.LENGTH_SHORT).show();
        }
    }



    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    // Interface Override Methods

    // Grocery Adapter
    @Override
    public void onGroceryItemChecked(String grocery, int position) {
        checkedGroceries.add(grocery);
        Log.i(TAG, "onGroceryItemChecked: Checked " + grocery + " at position " + position + ".");
    }

    @Override
    public void onGroceryItemUnchecked(String grocery, int position) {
        checkedGroceries.remove(grocery);
        Log.i(TAG, "onGroceryItemChecked: Unchecked " + grocery + " at position " + position + ".");
    }

    // SearchFragment OnRecipeSearch Interface
    @Override
    public void onRecipeSearch(String query) {
        // Do nothing if no query was entered
        if (query.isEmpty()) return;

        // Check for valid internet connection before attempting a search
        if (ConnectionHelper.validConnection(this)) {
            // Clear any caches of recipe searches
            mRecipeResults = new ArrayList<>();

            // Create a new DownloadTask to handle asynchronous downloading of the JSON data
            DownloadTask task = new DownloadTask(this);
            task.execute(query); // Execute task based on user-entered query

            // Show the Search Fragment
            ResultsFragment rF = ResultsFragment.newInstance(NavHelper.ResultType.SEARCH);
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, rF, ResultsFragment.TAG)
                    .addToBackStack(NavHelper.BACKSTACK_SEARCH)
                    .commit();
        } else {
            // Alert user they are not connected to the internet
            Toast.makeText(this, R.string.internet_to_search, Toast.LENGTH_SHORT).show();
        }
    }

    // SearchFragment
    @Override
    public RecipeResultAdapter getResultAdapter() {
        resultAdapter = new RecipeResultAdapter(this, mRecipeResults);
        return resultAdapter;
    }

    @Override
    public void onRecipeDownloaded(ResultRecipe recipe) {
        mRecipeResults.add(recipe);
        if (resultAdapter != null) resultAdapter.notifyDataSetChanged();
    }

    // DetailsFragment OnButtonClicked Interface
    @Override
    public void onButtonClicked(int btn_id, ResultRecipe recipe) {
        if (btn_id == R.id.btn_add_favorites) {
            // Favorites Button
            IOHelper.saveFavorite(this, recipe);
        } else if (btn_id == R.id.btn_add_grocery) {
            // Grocery Button
            for (String grocery : recipe.getIngredients()) IOHelper.saveGrocery(this, grocery);

            ArrayList<String> groceries = IOHelper.loadGroceries(this);
            for (String g : groceries) Log.i(TAG, "onButtonClicked: Grocery : " + g);
        }
    }

    // RecipeResultsAdapter OnRecipeCardClicked Interface
    @Override
    public void recipeCardViewClicked(ResultRecipe recipe) {
        // Set up new Details Fragment - pass in the recipe
        DetailsFragment detailsFragment = DetailsFragment.newInstance(recipe);

        // Display the fragment - add to Search backstack
        getFragmentManager().beginTransaction()
                .replace(R.id.container, detailsFragment, DetailsFragment.TAG)
                .addToBackStack(NavHelper.BACKSTACK_SEARCH)
                .commit();
    }

    // ResultsFragment OnResultsDisplayed Interface
    @Override
    public List<ResultRecipe> retrieveRecipeResults() {
        return mRecipeResults;
    }

    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////




    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    // App Drawer Navigation
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Outside helper class to handle Navigation Drawer selection(s)
        return NavHelper.drawerItemSelected(this, item,
                (DrawerLayout) this.findViewById(R.id.drawer_layout));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mNavToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////

}
