package com.fullsail.pp6.healthyhelper.classes;


// Zach Wilkin
// PP6 - 1709
// Macronutrients.java

import java.io.Serializable;

public class Macronutrients implements Serializable {

    // Member Variables
    private double calories;
    private double carbohydrate;
    private double fat;
    private double protein;


    // Constructor
    public Macronutrients(String calories, String carbs, String fat, String protein) {
        // Convert passed in String values to Double
        this.calories = Double.parseDouble(calories);
        this.carbohydrate = Double.parseDouble(carbs);
        this.fat = Double.parseDouble(fat);
        this.protein = Double.parseDouble(protein);
    }


    // Getters
    public double getCalories() {
        return calories;
    }
    public double getCarbohydrate() {
        return carbohydrate;
    }
    public double getFat() {
        return fat;
    }
    public double getProtein() {
        return protein;
    }
}
